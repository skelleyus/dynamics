using System;
using System.ComponentModel;

namespace Dynamics
{
    public class DynamicPropertyDescriptor : PropertyDescriptor
    {
        private readonly Type type;

        public DynamicPropertyDescriptor(string name, Type type)
            : base(name, null)
        {
            this.type = type;
        }

        public override string DisplayName
        {
            get { return string.Format("This: {0}", this.Name); }
        }

        public override bool CanResetValue(object component)
        {
            return false;
        }

        public override void ResetValue(object component) { }

        public override object GetValue(object component)
        {
            return null;
        }

        public override void SetValue(object component, object value) { }

        public override bool ShouldSerializeValue(object component)
        {
            return false;
        }

        public override Type ComponentType
        {
            get { return this.GetType(); }
        }

        public override bool IsReadOnly
        {
            get { return true; }
        }

        public override Type PropertyType
        {
            get { return type; }
        }
    }
}
