﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

using Dynamics.ViewModels;

namespace Dynamics
{
    public class DynamicObservableCollection<T> : ObservableCollection<dynamic>, ITypedList where T : BindableExpandoBase
    {
        public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
        {
            var properties = BindableExpandoBase.GetProperties(typeof(T));
            return new PropertyDescriptorCollection(properties.ToArray());
        }

        public string GetListName(PropertyDescriptor[] listAccessors)
        {
            return null;
        }
    }
}
